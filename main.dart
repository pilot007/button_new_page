import 'package:flutter/material.dart';
import 'dart:async';
import 'package:http/http.dart' as http;
import 'dart:convert';
void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(

        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'Muharrem Döner'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);


  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {

  String _value = '';
  String _value2 = '';

  final TextEditingController _textEditingControllerStart = new TextEditingController();
  final TextEditingController _textEditingControllerEnd = new TextEditingController();

  Future _selectDate() async {
    DateTime picked = await showDatePicker(
    context: context,
    initialDate: DateTime.now().add(Duration(hours: 1)),
    firstDate: DateTime.now(),
    lastDate: DateTime(2101));
    if(picked != null) setState(() => _value = picked.day.toString() +'.'+picked.month.toString() +'.'+picked.year.toString() );
    _textEditingControllerStart.text = _value;
  }

  Future _selectDate2() async {
    DateTime picked = await showDatePicker(
    context: context,
    initialDate: DateTime.now().add(Duration(hours: 1)),
    firstDate: DateTime.now(),
    lastDate: DateTime(2101));    
    if(picked != null) setState(() => _value2 = picked.day.toString() +'.'+picked.month.toString() +'.'+picked.year.toString() );
    _textEditingControllerEnd.text = _value2;  
  }
  
  Future<List<User>> _getUusers() async{
    var data = await http.get("https://jsonplaceholder.typicode.com/users");
    var jsonData = json.decode(data.body);
    List<User> users =[];
    for(var u in jsonData){
      User user = User(u["id"], u["name"], u["username"], u["email"]);
      users.add(user);
    }
    print(users.length);
    return users;
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(

        title: Text(widget.title),
      ),
      body: 
      new Container(
        padding: new EdgeInsets.all(10.0),
          child: new Column(
            children: <Widget>[
                 new TextField(
                  onTap: _selectDate,
                  controller: _textEditingControllerStart,
                  decoration: InputDecoration(
                  labelText: "Baslangıç tarihi",
                  icon: Icon(Icons.calendar_today),
                  border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(5.0),
              ),
            ),
            ),
              new Text("  "),
              new TextField(
                  onTap: _selectDate2,
                  controller: _textEditingControllerEnd,
                  decoration: InputDecoration(
                  labelText: "Bitiş Tarihi",
                  icon: Icon(Icons.calendar_today),
                  border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(5.0),
              ),
            ),
            ),
           
              new Row(
                children: <Widget>[
              new Text("  "),
              new RaisedButton(
              child: new Text('Rapor'),
              onPressed: () {
            // Navigate to second route when tapped.
              Navigator.push(context, MaterialPageRoute(builder: (context)=> DetailPage(_textEditingControllerStart.text,_textEditingControllerEnd.text)));
            },           
             color: Colors.red[400],
              shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(15.0)) ,),
                ]
                ),

            ],
        ),
        
      ),
            
    );
  }

}

class DetailPage extends StatelessWidget {
  final String sdate;
  final String edate;
  DetailPage(this.sdate, this.edate);
    Future<List<Sonuc>> _getUusers() async{
    print("sdate:"+sdate);
    print("edate:"+edate);
/*
    var data = await http.get("https://jsonplaceholder.typicode.com/users");
    var jsonData = json.decode(data.body);
    List<User> users =[];
    for(var u in jsonData){
      User user = User(u["id"], u["name"], u["username"], u["email"]);
      users.add(user);
    }
    print(users.length);
    return users;
*/
    var response = await http.get(
      Uri.encodeFull("http://159.146.30.92:81/donerci/api/values?startDate="+sdate+"&endDate="+sdate),
    );

    final jsonDatax  = json.decode(response.body.replaceAll("\"\"","\""));
    List<Sonuc> sonucs =[];
    //print(jsonDatax);
    
    String ss= "[{\"TARIH\":\"0001-01-01T00:00:00\",\"KOD\":\"0103\",\"ACIKLAMA\":\" GOBIT 100GR \",\"YEMEK_KODU\":\"01\",\"YEMEK_ADI\":\"DONERLER\",\"MIKTAR\":\"      137.00\",\"SFIY\":\"     1644.00\",\"ISKONTO\":\"        0.00\",\"NET_TUTAR\":\"     1644.00\"}, {\"TARIH\":\"0001-01-01T00:00:00\",\"KOD\":\"0103\",\"ACIKLAMA\":\" GOBIT 100GR \",\"YEMEK_KODU\":\"01\",\"YEMEK_ADI\":\"DONERLER\",\"MIKTAR\":\"      137.00\",\"SFIY\":\"     1644.00\",\"ISKONTO\":\"        0.00\",\"NET_TUTAR\":\"     1644.00\"}]"; 
    ss = response.body.replaceAll("\"\"","\"");
    var dd = json.decode(ss);
    var cc = json.decode(dd);

    for(var u in cc)
    {
    print(u["ACIKLAMA"]);
    Sonuc sonuc= Sonuc(KOD:u["KOD"],ACIKLAMA:u["ACIKLAMA"],
                       YEMEK_KODU: u["YEMEK_KODU"],YEMEK_ADI: u["YEMEK_ADI"],
                       MIKTAR: u["MIKTAR"],SFIY: u["SFIY"],
                       ISKONTO: u["ISKONTO"],NET_TUTAR: u["NET_TUTAR"]);
      sonucs.add(sonuc);
    }
    print( "data count : ");
    print( sonucs.length);
    
    return sonucs;

  }

  @override
  Widget build(BuildContext context) {
    
    return Scaffold(
      appBar: AppBar(

        title: Text("Satış Raporu"),
      ),
      body: Container(
        child: FutureBuilder(
          future: _getUusers(),
          builder: (BuildContext context, AsyncSnapshot snapshot){

            if(snapshot.data == null){
              return Container(
                child: Center(
                  child: Text('Loading'),
                ),
              );
            }else{
              return ListView.builder(
                  itemCount: snapshot.data.length,
                  itemBuilder: (BuildContext context, int index){
                    return ListTile(
                      title: Text(snapshot.data[index].YEMEK_ADI + " " +snapshot.data[index].ACIKLAMA),
                      subtitle: Text("Adet : "+snapshot.data[index].MIKTAR + "  -  Tutar : "+snapshot.data[index].NET_TUTAR),
                      onTap: (){
                        //Navigator.push(context, MaterialPageRoute(builder: (context)=> DetailPage(snapshot.data[index])));
                      },
                    );
                  }
              );
            }



          },
        ),
      ),

    );

  }
}


class User{
  final int id;
  final String name;
  final String username;
  final String email;


  User(this.id,this.name,this.username,this.email);
}


    class Sonuc
    {
        //final String TARIH;
        final String KOD;
        final String ACIKLAMA;
        final String YEMEK_KODU;
        final String YEMEK_ADI;
        final String MIKTAR;
        final String SFIY;
        final String ISKONTO;
        final String NET_TUTAR;
        
        Sonuc(
          {//this.TARIH,
          this.KOD,
          this.ACIKLAMA,
          this.YEMEK_KODU,
          this.YEMEK_ADI,
          this.MIKTAR,
          this.SFIY,
          this.ISKONTO,
          this.NET_TUTAR,});
     
     factory Sonuc.fromJson(Map<String, dynamic> parsedJson){
    return Sonuc(
      //TARIH : parsedJson['TARIH'],
      KOD : parsedJson ['KOD'],
      ACIKLAMA : parsedJson['ACIKLAMA'],
      YEMEK_KODU : parsedJson ['YEMEK_KODU'],      
      YEMEK_ADI : parsedJson['YEMEK_ADI'],
      MIKTAR : parsedJson ['MIKTAR'],
      SFIY : parsedJson['SFIY'],
      ISKONTO : parsedJson ['ISKONTO'],      
      NET_TUTAR : parsedJson ['NET_TUTAR'],      
    );
  }

     
    }


